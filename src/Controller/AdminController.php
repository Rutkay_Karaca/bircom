<?php

namespace App\Controller;

use App\Form\ProductFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Product;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use App\Services\ProductServiceInterface;
use App\Services\OrderServiceInterface;



class AdminController extends AbstractController
{

    public function __construct(ProductServiceInterface $productService,OrderServiceInterface $orderService)
    {
       $this->productservice=$productService;
       $this->orderservice=$orderService;
    }


    #[Route('/admin',name:'app_admin',methods:['GET'])]
    public function index(){

    $data=$this->productservice->getAllProducts();    
    return $this->render('admin/product/product.html.twig', ['products'=>$data]);

    }

    #[Route('/admin_new',name:'app_admin_new',methods:['GET','POST'])]
    public function addProduct(Request $request){
          
        $product = new Product();
        $form = $this->createForm(ProductFormType::class, $product);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
          $product = $form->getData();
          $this->productservice->createProduct($product); 
  
          return $this->redirectToRoute('app_admin');
        }

        return $this->render('admin/product/new_product.html.twig', [
            'newproductForm' => $form->createView(),
        ]);

    }

    #[Route('/admin_edit/{id}',name:'app_admin_edit',methods:['POST','GET'])]
    public function editProduct(Request $request,int $id){

        $product=$this->productservice->getProduct($id);

        $editform = $this->createForm(ProductFormType::class, $product);
        $editform->handleRequest($request);

        if($editform->isSubmitted() && $editform->isValid()) {

          $product=$editform->getData();
          $this->productservice->updateProduct($id,$product);
    
          return $this->redirectToRoute('app_admin');
          }
    
          return $this->render('admin/product/edit_product.html.twig', array(
            'editform' => $editform->createView()
          ));


    }

    #[Route('/admin_delete/{id}',name:'app_admin_delete',methods:['DELETE'])]
    public function deleteProduct(int $id):JsonResponse{

        $response=$this->productservice->deleteProduct($id);
        return new JsonResponse($response);
    }

    #[Route('/admin/orders',name:'app_admin_orders',methods:['GET','POST'])]
    public function listOrders(){

        $orders=$this->orderservice->orderList(); 
        return $this->render('admin/order.html.twig', ['orders'=>$orders]);

    }

    #[Route('/admin/order_confirm/{id}',name:'app_admin_order_confirm',methods:['GET','POST'])]
    public function confirmOrders($id){

        $this->orderservice->confirmOrder($id); 
        return $this->forward('App\Controller\AdminController::listOrders');

    }

}
