<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Order;
use App\Entity\OrdersProducts;


use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\Session\SessionInterface;
use App\Form\OrderFormType;
use App\Repository\OrdersProductsRepository;
use App\Services\OrderServiceInterface;
use App\Services\ProductServiceInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
class AppController extends AbstractController
{

    public function __construct(ProductServiceInterface $productService,OrderServiceInterface $orderService,SessionInterface $session,OrdersProductsRepository $op_em)
    {
        $this->productservice=$productService;
        $this->orderservice=$orderService;
        $this->session = $session;
        $this->normalizier=new Serializer([new ObjectNormalizer()]);
        $this->op_em=$op_em;
    }

    #[Route('/home',name:'app_home')]
    public function home(){
        
        $cart=$this->session->get('cart')??[];
        $data=$this->productservice->getAllProducts();   



        return $this->render('app/home.html.twig', ['products'=>$data,'cart'=>count($cart)]);
    }


    #[Route('/cart_add',name:'app_cart_add',methods:['POST'])]
    public function cartAdd(Request $request)
    {
        
        $data= $request->request->all(); 
        $productid=$data['product']; $quantity=$data['quantity'];

        $getproduct=$this->productservice->getProduct($productid);
        $getproduct=$this->normalizier->normalize($getproduct, 'null');

        $dizi=$this->session->get('cart')??[]; 
        
       if(array_key_exists($productid, $dizi)) {
       $ekle=$dizi[$productid]['quantity']; $dizi[$productid]['quantity']=$ekle+$quantity;       
       }
       else{
       $dizi[$productid]['quantity']=intval($quantity); 
       $dizi[$productid]['name']=$getproduct['name']; 
       $dizi[$productid]['price']=$getproduct['price'];  
       }        

       $this->session->set('cart',$dizi);

       return new JsonResponse(['Messeage'=>'oldu','count'=>count($dizi)]);
    }

    #[Route('/cart_remove/{id}',name:'app_cart_remove',methods:['GET'])]
    public function cartRemove($id)
    {
        

        $dizi=$this->session->get('cart')??[]; 

        
       if(array_key_exists($id, $dizi)) {
       unset($dizi[$id]);   $this->session->set('cart',$dizi);
       }
       return $this->forward('App\Controller\AppController::order');

    }

    #[Route('/order',name:'app_order',methods:['GET','POST'])]
    public function order(Request $request)
    {
        $cart=$this->session->get('cart')??[]; 


        $order=new Order();
        $form = $this->createForm(OrderFormType::class, $order);

        $form->handleRequest($request);

        if($form->isSubmitted() and !empty($cart) ) {
            $order = $form->getData(); 
            $total=0;


            $date=new \DateTime();
            $order->setCreatedAt($date);
            $order->setUser(1);
            $order->setStatus(0);
            $order->setCost(0);
            $order->setUser($this->get('security.token_storage')->getToken()->getUser());
            foreach ($cart as $key=>$row) {
            $order_product=new OrdersProducts();
            $product=$this->productservice->getProduct($key);
            $total=$total+($row['quantity']*$product->getPrice());
            $order_product->setProduct($product);
            $order_product->setOrder($order);
            $order_product->setQuantity($row['quantity']);

            $this->op_em->create($order_product);
            }

            $order->setCost($total);

            $response=$this->orderservice->createOrder($order);
            if($response['Messeage']=='New Order Created Successfully')
            {$this->session->set('cart',null);}
            
            return $this->redirectToRoute('app_home');

          }
  
          return $this->render('app/shopcart.html.twig', ['products'=>$cart,'form'=>$form->createView()]);
        
        

    }




}
