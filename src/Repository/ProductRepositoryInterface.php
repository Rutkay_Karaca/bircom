<?php

namespace App\Repository;

interface ProductRepositoryInterface
{
	
	public function getAllProducts();

    public function getProduct($id);

    public function deleteProduct($id);

    public function updateProduct($id,$data);

    public function createProduct($data);


}