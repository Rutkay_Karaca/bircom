<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Repository\ProductRepositoryInterface;


class ProductRepository extends ServiceEntityRepository implements ProductRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
        $this->er=$this;
        $this->em=$this->getEntityManager();
    }

	public function getAllProducts()
{    
    try{
    $data=$this->er->findAll(); 
    $response=$data;
    }catch (\Exception $e){
    $response=['Messeage'=>'Could not Get Products'];
    }
    return $response;
}

    public function getProduct($id){

    try{
    $product=$this->er->find($id); 
    $response=$product;
    }catch (\Exception $e){
    $response=['Messeage'=>'Failed'];
    }
    return $response;
    }


    public function updateProduct($id,$data)
{
    try{  
    $this->em->persist($data);
    $this->em->flush();
    $response=['Messeage'=>'Product Updated Successfully'];
    }catch (\Exception $e){
    $response=['Messeage'=>'Failed'];
    }
    return $response;
}
    public function createProduct($product)
{
    try{
    $this->em->persist($product);
    $this->em->flush();
    $response=['Messeage'=>'New Product Created Successfully'];
    }catch (\Exception $e){
    $response=['Messeage'=>'Failed'];
    }
    return $response;
}

    public function deleteProduct($id)
{
    try{
    $product=$this->er->find($id); 
    $this->em->remove($product);
    $this->em->flush(); 
    $response=['Messeage'=>'Deleted'];
    }catch (\Exception $e){
    $response=['Messeage'=>'Could not Delete Product'];
    }
    return $response;
}


}
