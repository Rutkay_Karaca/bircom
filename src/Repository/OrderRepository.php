<?php

namespace App\Repository;

use App\Entity\Order;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Repository\OrderRepositoryInterface;


class OrderRepository extends ServiceEntityRepository implements OrderRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Order::class);
        $this->er=$this;
        $this->em=$this->getEntityManager();
    }

    public function createOrder($order){
        try{
        $this->em->persist($order);
        $this->em->flush();
        $response=['Messeage'=>'New Order Created Successfully'];
        }catch (\Exception $e){ 
        $response=['Messeage'=>'Failed'];
        }
        return $response;
    }

    public function getPendingOrders()
    {
        $orders =$this->er->findBy(['status' => 0]);
        return $orders;
    }

    public function confirmOrder($id)
    {
        try{
        $order =$this->er->find($id);
        $order->setStatus(1);
        $this->em->persist($order);
        $this->em->flush();
        }catch (\Exception $e){ 

        }
        
    }
}
