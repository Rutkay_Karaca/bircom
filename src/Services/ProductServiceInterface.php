<?php
namespace App\Services;

interface ProductServiceInterface
{
	
	public function getAllProducts();

    public function getProduct($id);

    public function createProduct($data);

    public function updateProduct($id,$data);

    public function deleteProduct($id);

}