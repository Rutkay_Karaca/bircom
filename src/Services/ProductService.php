<?php
namespace App\Services;

use App\Repository\ProductRepositoryInterface;
use App\Services\ProductServiceInterface;

use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class ProductService implements ProductServiceInterface
{
	public function __construct(ProductRepositoryInterface $orderrepository,ValidatorInterface $validator)
	{
	   $this->repository = $orderrepository;
	   $this->validator=$validator;
	   $this->normalizier=new Serializer([new ObjectNormalizer()]);
	}

    public function getAllProducts(){
		$data=$this->repository->getAllProducts();
		$data=$this->normalizier->normalize($data, 'null');
		return $data;
	}

	public function getProduct($id){
		$product=$this->repository->getProduct($id);
		return $product;

	}

    public function createProduct($data){
		$errors = $this->validator->validate($data);
        if (count($errors) > 0) {
        $errorsString = (string) $errors;     
		$response=['messeage'=>$errorsString];
        }
		else{
		$data = $this->repository->createProduct($data);
		$response=$data;
		}

        return $response;
	}

    public function updateProduct($id,$data){
		$errors = $this->validator->validate($data);
        if (count($errors) > 0) {
        $errorsString = (string) $errors;      
		$response=['messeage'=>$errorsString];
        }
		else{		
		$response=$this->repository->updateProduct($id,$data);
		}

        return $response;
	}

    public function deleteProduct($id){
		$response = $this->repository->deleteProduct($id);
		return $response;
	}


}