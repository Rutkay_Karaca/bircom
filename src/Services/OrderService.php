<?php
namespace App\Services;

use App\Repository\OrderRepositoryInterface;
use App\Services\OrderServiceInterface;

use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class OrderService implements OrderServiceInterface
{
    public function __construct(OrderRepositoryInterface $orderrepository,ValidatorInterface $validator)
	{
	   $this->repository = $orderrepository;
	   $this->validator=$validator;
	   $this->normalizier=new Serializer([new DateTimeNormalizer(),new ObjectNormalizer()]);
	}
    
    public function createOrder($data){
        $errors = $this->validator->validate($data);
        if (count($errors) > 0) {
        $errorsString = (string) $errors;      dd($errorsString);
		$response=['messeage'=>$errorsString];
        }
		else{
		$data = $this->repository->createOrder($data);
		$response=$data;
		}

        return $response;

    }

	public function orderList(){
	$orders=$this->repository->getPendingOrders();
	$orders=$this->normalizier->normalize($orders, 'null'); 
	return $orders;	
	}

	public function confirmOrder($id){
	$this->repository->confirmOrder($id);
	}

}