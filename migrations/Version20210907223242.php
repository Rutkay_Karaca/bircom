<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210907223242 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE orders_products ADD product_id INT NOT NULL, ADD quantity INT NOT NULL');
        $this->addSql('ALTER TABLE orders_products ADD CONSTRAINT FK_749C879C8D9F6D38 FOREIGN KEY (order_id) REFERENCES `order` (id)');
        $this->addSql('ALTER TABLE orders_products ADD CONSTRAINT FK_749C879C4584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('CREATE INDEX IDX_749C879C8D9F6D38 ON orders_products (order_id)');
        $this->addSql('CREATE INDEX IDX_749C879C4584665A ON orders_products (product_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE `orders_products` DROP FOREIGN KEY FK_749C879C8D9F6D38');
        $this->addSql('ALTER TABLE `orders_products` DROP FOREIGN KEY FK_749C879C4584665A');
        $this->addSql('DROP INDEX IDX_749C879C8D9F6D38 ON `orders_products`');
        $this->addSql('DROP INDEX IDX_749C879C4584665A ON `orders_products`');
        $this->addSql('ALTER TABLE `orders_products` DROP product_id, DROP quantity');
    }
}
